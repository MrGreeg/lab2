#pragma once
#include <string>				// ����� ������� ���������� � ������������ ���

const int MAX_COUNT_STATES = 256;	// ������������ ���������� �������� �������

using namespace std;

struct Signal
{
	char level = 0;		// ������� ������� (0 ��� 1)
	unsigned char duration = 0;	// ������������ ������� (�������� 255)
	Signal* next = nullptr;
	Signal* prev = nullptr;
};

class BinarySignalDinamic
{

public:
	BinarySignalDinamic();		// ������ �����������

	BinarySignalDinamic(char level);

	BinarySignalDinamic(const string &str);

	BinarySignalDinamic& operator !();  // �������������� �� ��������������� �����

	friend std::ostream& operator <<(std::ostream &out, const BinarySignalDinamic& binarySignal);

	friend std::istream& operator >>(std::istream& in, BinarySignalDinamic& binarySignal);

	friend BinarySignalDinamic& operator +=(BinarySignalDinamic& left, const BinarySignalDinamic& right); // ���������� ����������� �� ��������� ����� ������� �� ���� ����������� ���������
	
	friend BinarySignalDinamic& operator *=(BinarySignalDinamic& left, const int& right);

	BinarySignalDinamic& operator ()(const int& position, const char& value);

private:
	Signal *signals_ = nullptr;
	int size_;

	Signal* signals_end = nullptr;		// ��� ��������� ������� � ����� ������
	void push(char value, char duration, int position = -1);	// ����� ������� � ���������� ������
	void pop(int position=-1);										// ����� ���������� �� ����������� ������
};

