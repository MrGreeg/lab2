﻿#include "BinarySignalDinamic.h"
#include <iostream>

BinarySignalDinamic::BinarySignalDinamic() {
	size_ = 0;
}

BinarySignalDinamic::BinarySignalDinamic(const BinarySignalDinamic& binarySignalDinamic) {
	BinarySignalDinamic::size_ = 0;
	Signal *temp = binarySignalDinamic.signals_;
	while (temp != nullptr) {
		BinarySignalDinamic::push(temp->level, temp->duration);
	}
}


BinarySignalDinamic::BinarySignalDinamic(char level) {
	size_ = MAX_COUNT_STATES;
	if (level > 0) {
		level = 1;
	}
	for (int i = 0; i < MAX_COUNT_STATES; i++) {
		push(level, 255);
	}
}

BinarySignalDinamic::BinarySignalDinamic(const string &inputSignal) {
	size_ = 0;
	if (inputSignal.length() > 0) {
		int duration = 1;
		char level = inputSignal[0] > 0 ? 1 : 0;
		char levelSignal = level;
		for (int i = 1; i < inputSignal.length(); ++i) {
			levelSignal = inputSignal[i] > '0' ? 1 : 0;
			if (level == levelSignal) {
				if (duration == 255) {
					push(level, duration);
					duration = 0;
				}
				else {
					duration++;
				}
			}
			else {
				push(level, duration);
				duration = 1;
				level = levelSignal;
			}
		}
		push(level, duration);
	}
}

BinarySignalDinamic& BinarySignalDinamic::operator!() {		
	BinarySignalDinamic tempBinarySignal = *this;
	Signal* temp = tempBinarySignal.signals_;
	while (temp != nullptr) {
		temp->level= (temp->level + 1) % 2;	// Инверсия значения сигнала
	}
	return tempBinarySignal;
}

BinarySignalDinamic& BinarySignalDinamic::operator()(const int& position, const char& value) {	// вставка
	if (this->size_ < MAX_COUNT_STATES) {
		if (position > this->size_) {
			throw "Out of the acceptable range";
		}
		int setBlock = -1;
		int len = 0;
		char level = value > 0 ? 1 : 0;
		while (len < position) {
			setBlock++;
			len += this->signals_[setBlock].duration;
		}
		if (this->signals_[setBlock].level == level) {
			while (setBlock + 1 < size_ - 1 && this->signals_[setBlock + 1].level == level) {
				setBlock++;
			}
			if (this->signals_[setBlock].duration < 255) {
				this->signals_[setBlock].duration++;
			}
			else
			{
				for (int i = size_; i > setBlock; i--) {
					this->signals_[i] = this->signals_[i - 1];
				}
				this->signals_[setBlock].level = level;
				this->signals_[setBlock].duration = 1;
			}
		}
		else
		{
			for (int i = size_; i > setBlock; i--) {
				this->signals_[i] = this->signals_[i - 1];
			}
			this->signals_[setBlock].level = level;
			this->signals_[setBlock].duration = 1;
		}
	}
	else {
		throw "Exceeding the maximum size";
	}
}

void BinarySignalDinamic::push(char value, char duration, int position = -1) {
	Signal *temp = new Signal();
	temp->duration = duration;
	temp->level = value > 0 ? 1 : 0;
	if (position == -1 || position >= size_) {
		if (size_ == 0) {
			signals_ = temp;
			signals_end = signals_;
		}
		else {
			temp->prev = signals_end;
			signals_end = temp;
		}
	}
	else {
		if (position == 0) {
			temp->next = signals_;
			signals_->prev = temp;
		}
		else {
			Signal* element = signals_;
			for (int i = 0; i < position; i++) {
				element = element->next;
			}
			temp->next = element->next;
			element->next = temp;
			temp->prev = temp->next->prev;
			temp->next->prev = temp;
		}
		
	}
	size_++;
}

void BinarySignalDinamic::pop(int position=-1)
{
	if (position > size_ - 1 || position==0) {
		position = -1;
	}
	if (position == -1) {
		Signal* temp = signals_;
		signals_ = signals_->next;
		signals_->prev = nullptr;
		delete temp;
	}
	else {
		Signal* temp = signals_;
		int setBlock = 0;
		while (setBlock < position) {
			setBlock++;
			temp = temp->next;
		}
		temp->prev->next = temp->next;
		if (temp->next != nullptr) {
			temp->next->prev = temp->prev;
		}
		delete temp;
	}
	size_--;
}

std::ostream& operator<<(std::ostream &out, const BinarySignalDinamic& binarySignal)
{
	char symbol;
	Signal* temp = binarySignal.signals_;
	Signal* tempi_1 = binarySignal.signals_;
	while (temp != nullptr) {
		if (temp->level == 0) {
			symbol = '_';
		}
		else {
			symbol = char(196);
		}
		if (temp != binarySignal.signals_ && tempi_1->level != temp->level) {
			if (temp->level == 0) {
				out << char(191);
			}
			else {
				out << char(218);
			}
		}
		for (int j = 0; j < temp->duration; ++j) {
			out << symbol;
		}
		tempi_1 = temp;
		temp = temp->next;
	}
	return out;
}	

std::istream& operator>> (std::istream &in, BinarySignalDinamic&binarySignal)
{
	//if (binarySignal.size_ < MAX_COUNT_STATES) {
		try {
			BinarySignalDinamic tempBinarySignal = binarySignal;
			char inputSignal;
			in >> inputSignal;
			int duration = 1;
			char level = inputSignal > 0 ? 1 : 0;
			char levelSignal = level;
			while (!in.eof()) {
				in >> inputSignal;
				levelSignal = inputSignal > '0' ? 1 : 0;
				if (level == levelSignal) {
					if (duration == 255) {
						tempBinarySignal.push(level, duration);
						duration = 0;
					}
					else {
						duration++;
					}
				}
				else {
					tempBinarySignal.push(level, duration);
					duration = 1;
					level = levelSignal;
				}
			}
			tempBinarySignal.push(level, duration);

			binarySignal = tempBinarySignal;
		}
		catch (std::ios_base::iostate except){
			in.setstate(ios_base::failbit);
			throw "Exceeding input stream";
		}
	/*}
	else {
		throw "Exceeding the maximum size";
	}*/
	return in;
}

BinarySignalDinamic& operator+=(BinarySignalDinamic& left, const BinarySignalDinamic& right)// исклюючения по ограничению длины / сделать методом
{	
	//if ((left.signals_[left.size_ - 1].level == right.signals_[0].level) && 
	//	(left.signals_[left.size_ - 1].duration + right.signals_[0].duration > 255)) {
	//	if (left.size_ + right.size_ - 1 > MAX_COUNT_STATES) {
	//		throw "Exceeding the maximum size";
	//	}
	//}
	//else {
	//	if (left.size_ + right.size_ > MAX_COUNT_STATES) {
	//		throw "Exceeding the maximum size";
	//	}
	//}

	//int countElement = (left.size_ + right.size_) % MAX_COUNT_STATES;
	Signal* temp = right.signals_;
	while (temp != nullptr) {
		left.push(temp->level, temp->duration);
		temp = temp->next;
	}
	return left;
}

BinarySignalDinamic& operator*=(BinarySignalDinamic& left, const int& right)
{
	//if ((left.signals_[left.size_ - 1].level == left.signals_[0].level) &&
	//	(left.signals_[left.size_ - 1].duration + left.signals_[0].duration > 255)) {
	//	if (left.size_ * 2 - right - 1 > MAX_COUNT_STATES) {
	//		throw "Exceeding the maximum size";
	//	}
	//}
	//else {
	//	if (left.size_ * 2 > MAX_COUNT_STATES) {
	//		throw "Exceeding the maximum size";
	//	}
	//}

	Signal *endElem = left.signals_end;

	for (int n = 0; n < right; ++n) {
		Signal* temp = left.signals_;
		while (temp != endElem) {
			left.push(temp->level, temp->duration);
			temp = temp->next;
		}
		left.push(temp->level, temp->duration);
	}
}




// unit тесты