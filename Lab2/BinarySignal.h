#pragma once
#include <string>				// ����� ������� ���������� � ������������ ���

const int MAX_COUNT_STATES = 256;	// ������������ ���������� �������� �������

using namespace std;

struct Signal
{
	char level = 0;		// ������� ������� (0 ��� 1)
	unsigned char duration = 0;	// ������������ ������� (�������� 255)
};

class BinarySignal
{

public:
	BinarySignal();		// ������ �����������

	BinarySignal(char level);

	BinarySignal(const string &str);

	BinarySignal& operator !();  // �������������� �� ��������������� �����

	friend std::ostream& operator <<(std::ostream &out, const BinarySignal& binarySignal);

	friend std::istream& operator >>(std::istream& in, BinarySignal& binarySignal);

	friend BinarySignal& operator +=(BinarySignal& left, const BinarySignal& right); // ���������� ����������� �� ��������� ����� ������� �� ���� ����������� ���������
	
	friend BinarySignal& operator *=(BinarySignal& left, const int& right);

	BinarySignal& operator ()(const int& position, const char& value);

private:
	Signal signals_[MAX_COUNT_STATES];
	int size_;
};

