﻿#include "BinarySignal.h"
#include <iostream>

BinarySignal::BinarySignal() {
	size_ = 0;
}

BinarySignal::BinarySignal(char level) {
	size_ = MAX_COUNT_STATES;
	if (level > 0) {
		level = 1;
	}
	for (int i = 0; i < MAX_COUNT_STATES; i++) {
		signals_[i].level = level;
		signals_[i].duration = 255;
	}
}

BinarySignal::BinarySignal(const string &inputSignal) {
	size_ = 0;
	if (inputSignal.length() > 0) {
		int duration = 1;
		char level = inputSignal[0] > 0 ? 1 : 0;
		char levelSignal = level;
		for (int i = 1; i < inputSignal.length(); ++i) {
			levelSignal = inputSignal[i] > '0' ? 1 : 0;
			if (level == levelSignal) {
				if (duration == 255) {
					signals_[size_].level = level;
					signals_[size_++].duration = duration;
					duration = 0;
				}
				else {
					duration++;
				}
			}
			else {
				signals_[size_].level = level;
				signals_[size_++].duration = duration;
				duration = 1;
				level = levelSignal;
			}
		}
		signals_[size_].level = level;
		signals_[size_++].duration = duration;
	}
}

BinarySignal& BinarySignal::operator!() {
	BinarySignal tempBinarySignal = *this;
	for (int i = 0; i < size_; ++i) {
		tempBinarySignal.signals_[i].level = (signals_[i].level + 1) % 2;	// Инверсия значения сигнала
	}
	return tempBinarySignal;
}

BinarySignal& BinarySignal::operator()(const int& position, const char& value) {	// вставка
	if (this->size_ < MAX_COUNT_STATES) {
		if (position > this->size_) {
			throw "Out of the acceptable range";
		}
		int setBlock = -1;
		int len = 0;
		char level = value > 0 ? 1 : 0;
		while (len < position) {
			setBlock++;
			len += this->signals_[setBlock].duration;
		}
		if (this->signals_[setBlock].level == level) {
			while (setBlock + 1 < size_ - 1 && this->signals_[setBlock + 1].level == level) {
				setBlock++;
			}
			if (this->signals_[setBlock].duration < 255) {
				this->signals_[setBlock].duration++;
			}
			else
			{
				for (int i = size_; i > setBlock; i--) {
					this->signals_[i] = this->signals_[i - 1];
				}
				this->signals_[setBlock].level = level;
				this->signals_[setBlock].duration = 1;
			}
		}
		else
		{
			for (int i = size_; i > setBlock; i--) {
				this->signals_[i] = this->signals_[i - 1];
			}
			this->signals_[setBlock].level = level;
			this->signals_[setBlock].duration = 1;
		}
	}
	else {
		throw "Exceeding the maximum size";
	}
}

std::ostream& operator<<(std::ostream &out, const BinarySignal& binarySignal)
{
	char symbol;
	for (int i = 0; i < binarySignal.size_; ++i) {
		if (binarySignal.signals_[i].level == 0) {
			symbol = '_';
		}
		else {
			symbol = char(196);
		}
		if (i > 0 && binarySignal.signals_[i - 1].level != binarySignal.signals_[i].level) {
			if (binarySignal.signals_[i].level == 0) {
				out << char(191);
			}
			else {
				out << char(218);
			}
		}
		for (int j = 0; j < binarySignal.signals_[i].duration; ++j) {
			out << symbol;
		}
	}
	return out;
}	

std::istream& operator>> (std::istream &in, BinarySignal&binarySignal) // исправить
{
	if (binarySignal.size_ < MAX_COUNT_STATES) {
		try {
			BinarySignal tempBinarySignal = binarySignal;
			char inputSignal;
			in >> inputSignal;
			int duration = 1;
			char level = inputSignal > 0 ? 1 : 0;
			char levelSignal = level;
			while (!in.eof()) {
				in >> inputSignal;
				levelSignal = inputSignal > '0' ? 1 : 0;
				if (level == levelSignal) {
					if (duration == 255) {
						tempBinarySignal.signals_[tempBinarySignal.size_].level = level;
						tempBinarySignal.signals_[tempBinarySignal.size_++].duration = duration;
						duration = 0;
					}
					else {
						duration++;
					}
				}
				else {
					tempBinarySignal.signals_[tempBinarySignal.size_].level = level;
					tempBinarySignal.signals_[tempBinarySignal.size_++].duration = duration;
					duration = 1;
					level = levelSignal;
				}
			}
			tempBinarySignal.signals_[tempBinarySignal.size_].level = level;
			tempBinarySignal.signals_[tempBinarySignal.size_++].duration = duration;

			binarySignal = tempBinarySignal;
		}
		catch (std::ios_base::iostate except){
			in.setstate(ios_base::failbit);
			throw "Exceeding input stream";
		}
	}
	else {
		throw "Exceeding the maximum size";
	}
	return in;
}

BinarySignal& operator+=(BinarySignal& left, const BinarySignal& right)// исклюючения по ограничению длины / сделать методом
{	
	if ((left.signals_[left.size_ - 1].level == right.signals_[0].level) && 
		(left.signals_[left.size_ - 1].duration + right.signals_[0].duration > 255)) {
		if (left.size_ + right.size_ - 1 > MAX_COUNT_STATES) {
			throw "Exceeding the maximum size";
		}
	}
	else {
		if (left.size_ + right.size_ > MAX_COUNT_STATES) {
			throw "Exceeding the maximum size";
		}
	}

	int countElement = (left.size_ + right.size_) % MAX_COUNT_STATES;
	for (int i = 0; i < countElement; ++i) {
		left.signals_[left.size_].level = right.signals_[i].level;
		left.signals_[left.size_++].duration = right.signals_[i].duration;
	}
	return left;
}

BinarySignal& operator*=(BinarySignal& left, const int& right)
{
	if ((left.signals_[left.size_ - 1].level == left.signals_[0].level) &&
		(left.signals_[left.size_ - 1].duration + left.signals_[0].duration > 255)) {
		if (left.size_ * 2 - right - 1 > MAX_COUNT_STATES) {
			throw "Exceeding the maximum size";
		}
	}
	else {
		if (left.size_ * 2 > MAX_COUNT_STATES) {
			throw "Exceeding the maximum size";
		}
	}

	int size = left.size_;
	for (int n = 0; n < right; ++n) {
		for (int i = 0; i < size && left.size_<MAX_COUNT_STATES; ++i) {
			left.signals_[left.size_].level = left.signals_[i].level;
			left.signals_[left.size_++].duration = left.signals_[i].duration;
		}
	}
}




// unit тесты